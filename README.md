# Log-sized Proof of 1-of-N knowledge of DL

Ian Goldberg (iang@uwaterloo.ca), 2020-04-20

This code implements the protocol of Section 3 of Groth and Kohlweiss,
["One-out-of-Many Proofs: Or How to Leak a Secret and Spend a Coin"](https://eprint.iacr.org/2014/764.pdf),
Eurocrypt 2015.

You can look at this protocol in a number of ways:

  * knowledge of an opening of one of N Pedersen commitments to 0
  * knowledge of one of N discrete logs of group elements
  * knowledge of one of N private keys

The technique of Section 4 of the above paper uses this same protocol
to produce a log-sized ring signature: just compute the challenge
x non-interactively as a hash of the group parameters, a message,
the ring of public keys, and the output of the prover's first message.

The size of the proof is 4n group elements, plus 3n+1 scalars, where
n = ceil(log\_2(N)), and N is the number of commitments/group
elements/public keys.
