package main

import (
	"fmt"
	"os"
	"strconv"
	"math/big"
	"go.dedis.ch/kyber"
	"go.dedis.ch/kyber/pairing/bn256"
	"go.dedis.ch/kyber/util/random"
)

func main() {
	/* Command-line arg is N, the number of commitments */
	var N int
	var err error
	if len(os.Args) > 1 {
		N, err = strconv.Atoi(os.Args[1])
		if err != nil {
			panic(err)
		}
	} else {
		N = 25
	}
	if (N < 2) {
		panic("N must be at least 2")
	}
	N32 := uint32(N)
	group := bn256.NewSuite().G1()
	rand := random.New()
	params := GroupParams {
		group,
		group.Point().Pick(rand),
		group.Point().Pick(rand),
		group.Point().Pick(rand),
	}

	// Create an array of commitments, one of which is a commitment
	// to 0
	Cs := make([]kyber.Point, N32)

	// Which one do we know?  Pick a random 0 <= ell < N
	Nbig := big.NewInt(int64(N))
	ell := uint32(random.Int(Nbig, rand).Int64())
	fmt.Printf("ell = %d\n", ell)
	var privkey kyber.Scalar
	var i uint32
	for i = 0; i < N32; i++ {
		if i == ell {
			privkey = group.Scalar().Pick(rand)
			Cs[i] = group.Point().Mul(privkey, params.B)
		} else {
			Cs[i] = group.Point().Pick(rand)
		}
	}

	pub, priv := ProofStep1(params, Cs, ell, privkey)
	x := GenChallenge(params, pub)
	proof := ProofStep2(params, priv, x)
	if Verify(params, Cs, pub, x, proof) {
		fmt.Printf("SUCCESS\n")
	} else {
		fmt.Printf("VERIFICATION FAILED\n")
	}
}
